EESchema Schematic File Version 2  date Wed Oct 10 00:52:24 2012
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:bs170f
LIBS:zxmhc3f381n8
LIBS:cb-h_bridge-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "10 oct 2012"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6150 3950 6250 3950
Wire Wire Line
	6250 3750 6150 3750
Wire Wire Line
	6150 4450 6250 4450
Wire Wire Line
	4150 3200 3450 3200
Connection ~ 3450 3500
Wire Wire Line
	3300 3500 3450 3500
Wire Wire Line
	3550 4150 3300 4150
Wire Wire Line
	3450 3200 3450 4250
Wire Wire Line
	3400 3950 3400 5000
Wire Wire Line
	3400 3950 3550 3950
Wire Wire Line
	3850 3500 4050 3500
Wire Wire Line
	4950 4700 4950 4250
Wire Wire Line
	5350 4050 4950 4050
Wire Wire Line
	5200 3550 5200 3500
Wire Wire Line
	5200 3500 4450 3500
Wire Wire Line
	4950 3950 5050 3950
Wire Wire Line
	5050 3950 5050 3500
Connection ~ 5050 3500
Wire Wire Line
	4950 4150 5350 4150
Wire Wire Line
	5200 4050 5200 4200
Connection ~ 5200 4050
Wire Wire Line
	4450 4700 5200 4700
Connection ~ 4950 4700
Wire Wire Line
	4050 4700 3850 4700
Wire Wire Line
	3450 4250 3550 4250
Wire Wire Line
	3550 4050 3300 4050
Wire Wire Line
	3300 4700 3400 4700
Connection ~ 3400 4700
Wire Wire Line
	3400 5000 4150 5000
Wire Wire Line
	6250 4250 6150 4250
Wire Wire Line
	6150 3650 6250 3650
Wire Wire Line
	6150 3850 6250 3850
Text GLabel 6150 3850 0    60   Output ~ 0
IN2
Text GLabel 6150 3750 0    60   Output ~ 0
IN1
Text GLabel 6150 3950 0    60   BiDi ~ 0
GMTR
Text GLabel 6150 3650 0    60   Output ~ 0
VMTR
Text GLabel 6150 4450 0    60   Input ~ 0
OUT1
Text GLabel 6150 4250 0    60   Input ~ 0
OUT2
$Comp
L CONN_2 P2
U 1 1 5074EF16
P 6600 4350
F 0 "P2" V 6550 4350 40  0000 C CNN
F 1 "CONN_2" V 6650 4350 40  0000 C CNN
	1    6600 4350
	1    0    0    -1  
$EndComp
$Comp
L CONN_4 P1
U 1 1 5074EF07
P 6600 3800
F 0 "P1" V 6550 3800 50  0000 C CNN
F 1 "CONN_4" V 6650 3800 50  0000 C CNN
	1    6600 3800
	1    0    0    -1  
$EndComp
Text GLabel 3850 4700 0    60   BiDi ~ 0
GMTR
$Comp
L R R1
U 1 1 5074E412
P 5200 3800
F 0 "R1" V 5280 3800 50  0000 C CNN
F 1 "2.2k" V 5200 3800 50  0000 C CNN
	1    5200 3800
	1    0    0    -1  
$EndComp
Text GLabel 5350 4050 2    60   Input ~ 0
VMTR
Text GLabel 3300 4150 0    60   BiDi ~ 0
GMTR
$Comp
L ZXMHC3F381N8 U1
U 1 1 5074E8BB
P 4300 4500
F 0 "U1" H 4300 4500 60  0000 C CNN
F 1 "ZXMHC3F381N8" H 4250 5200 60  0000 C CNN
	1    4300 4500
	1    0    0    -1  
$EndComp
Text GLabel 5350 4150 2    60   Output ~ 0
OUT2
Text GLabel 3300 4050 0    60   Output ~ 0
OUT1
Text GLabel 3850 3500 0    60   BiDi ~ 0
GMTR
Text GLabel 3300 4700 0    60   Input ~ 0
IN2
Text GLabel 3300 3500 0    60   Input ~ 0
IN1
$Comp
L R R2
U 1 1 5074E417
P 5200 4450
F 0 "R2" V 5280 4450 50  0000 C CNN
F 1 "2.2k" V 5200 4450 50  0000 C CNN
	1    5200 4450
	1    0    0    -1  
$EndComp
$Comp
L BS170F Q1
U 1 1 5074E3BB
P 4250 3400
F 0 "Q1" H 4250 3590 30  0000 R CNN
F 1 "BS170F" H 4250 3220 30  0000 R CNN
	1    4250 3400
	0    1    1    0   
$EndComp
$Comp
L BS170F Q2
U 1 1 5074E2F9
P 4250 4800
F 0 "Q2" H 4250 4990 30  0000 R CNN
F 1 "BS170F" H 4250 4620 30  0000 R CNN
	1    4250 4800
	0    1    -1   0   
$EndComp
$EndSCHEMATC
