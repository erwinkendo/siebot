PCBNEW-LibModule-V1  lun 18 mar 2013 21:27:11 COT
# encoding utf-8
$INDEX
Q-CSD
$EndINDEX
$MODULE Q-CSD
Po 0 0 0 15 5147CCE3 5147CCA0 ~~
Li Q-CSD
Sc 5147CCA0
AR 
Op 0 0 0
T0 1010 -1350 600 600 0 120 N V 21 N "Q-CSD"
T1 -1890 -1230 600 600 0 120 N V 21 N "VAL**"
DS -760 -180 760 -180 59 21
DS 760 -180 760 940 59 21
DS 760 940 -760 940 59 21
DS -760 940 -760 -180 59 21
$PAD
Sh "1" R 248 197 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -565 0
.LocalClearance 6
$EndPAD
$PAD
Sh "2" R 248 197 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -565 256
.LocalClearance 6
$EndPAD
$PAD
Sh "3" R 248 197 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -565 512
.LocalClearance 6
$EndPAD
$PAD
Sh "4" R 248 197 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -565 768
.LocalClearance 6
$EndPAD
$PAD
Sh "5" R 248 197 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 565 768
.LocalClearance 6
$EndPAD
$PAD
Sh "6" R 248 197 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 565 512
.LocalClearance 6
$EndPAD
$PAD
Sh "7" R 248 197 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 565 256
.LocalClearance 6
$EndPAD
$PAD
Sh "8" R 248 197 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 565 0
.LocalClearance 6
$EndPAD
$EndMODULE  Q-CSD
$EndLIBRARY
