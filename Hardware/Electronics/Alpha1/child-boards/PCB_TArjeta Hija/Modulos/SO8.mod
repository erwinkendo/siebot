PCBNEW-LibModule-V1  mar 19 mar 2013 03:09:00 COT
# encoding utf-8
$INDEX
SO8
$EndINDEX
$MODULE SO8
Po 0 0 0 15 51481D12 00000000 ~~
Li SO8
Sc 00000000
AR SO8
Op 0 0 0
T0 -970 2610 600 600 0 120 N V 21 N "SO8"
T1 1590 2640 600 600 0 120 N V 21 N "VAL**"
DS -510 70 -340 -140 59 21
DS 590 -190 590 1670 150 21
DS -570 -200 570 -200 150 21
DS 580 1660 -570 1660 150 21
DS -570 1660 -570 -200 150 21
$PAD
Sh "1" R 701 283 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -921 0
.LocalClearance 6
$EndPAD
$PAD
Sh "2" R 701 283 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -921 500
.LocalClearance 6
$EndPAD
$PAD
Sh "3" R 701 283 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -930 1000
.LocalClearance 6
$EndPAD
$PAD
Sh "4" R 701 283 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -921 1500
.LocalClearance 6
$EndPAD
$PAD
Sh "8" R 701 283 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 921 0
.LocalClearance 6
$EndPAD
$PAD
Sh "7" R 701 283 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 921 500
.LocalClearance 6
$EndPAD
$PAD
Sh "6" R 701 283 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 921 1000
.LocalClearance 6
$EndPAD
$PAD
Sh "5" R 701 283 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 921 1500
.LocalClearance 6
$EndPAD
$EndMODULE  SO8
$EndLIBRARY
