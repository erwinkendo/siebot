EESchema Schematic File Version 2  date Fri 19 Apr 2013 17:10:57 COT
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ipc-7351-transistor
LIBS:LM3485
LIBS:transistor-arrays
LIBS:mc74hc595a
LIBS:PCB-cache
LIBS:irf9333
LIBS:mcp73831
LIBS:usb_a
LIBS:usba-plug
LIBS:usbconn
LIBS:usb-mini
LIBS:con-jack
LIBS:csd16323q3
LIBS:MCP73837
LIBS:baw101
LIBS:ovsrrgbcc3
LIBS:mcp6l04
LIBS:irf5803
LIBS:mcp6l02
LIBS:xc6216
LIBS:bat_con
LIBS:mbrb2515lt4g
LIBS:lf50cdt
LIBS:lm3478
LIBS:PCB-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 4
Title ""
Date "19 apr 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 5600 5600 2    45   Input ~ 0
NI
Text GLabel 7200 6300 2    45   Input ~ 0
NI
Text GLabel 6150 6300 0    45   Input ~ 0
GND
Text GLabel 4500 5600 0    45   Input ~ 0
GND
Text GLabel 7200 6400 2    45   Input ~ 0
TX
Text GLabel 6150 6400 0    45   Input ~ 0
RX
Wire Wire Line
	8550 5900 8675 5900
Wire Wire Line
	8675 5700 8550 5700
Wire Wire Line
	7650 5800 7750 5800
Wire Wire Line
	4400 6100 4650 6100
Wire Wire Line
	6275 6400 6150 6400
Wire Wire Line
	7075 6400 7200 6400
Wire Wire Line
	4500 6000 4650 6000
Wire Wire Line
	4650 5800 4500 5800
Wire Wire Line
	4650 5600 4500 5600
Wire Wire Line
	5600 5700 5450 5700
Wire Wire Line
	5450 5900 5600 5900
Wire Wire Line
	9275 4650 9100 4650
Wire Wire Line
	9200 3975 9300 3975
Wire Wire Line
	9300 3775 9200 3775
Wire Wire Line
	9000 3200 9150 3200
Wire Wire Line
	9000 3000 9150 3000
Wire Wire Line
	8200 3200 8050 3200
Wire Wire Line
	8200 3000 8050 3000
Wire Wire Line
	6250 4700 6150 4700
Wire Wire Line
	6150 4500 6250 4500
Wire Wire Line
	6250 4150 6150 4150
Wire Wire Line
	6250 3950 6150 3950
Wire Wire Line
	6250 3750 6150 3750
Wire Wire Line
	6100 3200 6250 3200
Wire Wire Line
	7050 3100 7200 3100
Wire Wire Line
	7050 2700 7200 2700
Wire Wire Line
	6250 2700 6100 2700
Wire Wire Line
	8600 1750 8750 1750
Wire Wire Line
	9550 1950 9700 1950
Wire Wire Line
	9550 1750 9700 1750
Wire Wire Line
	9550 1450 9700 1450
Wire Wire Line
	8750 1550 8600 1550
Wire Wire Line
	9550 1350 9700 1350
Wire Wire Line
	6200 1200 6000 1200
Wire Wire Line
	6200 1400 6000 1400
Wire Wire Line
	6200 1700 6000 1700
Wire Wire Line
	6200 1900 6000 1900
Wire Wire Line
	7000 2000 7200 2000
Wire Wire Line
	7000 1800 7200 1800
Wire Wire Line
	7000 1500 7200 1500
Wire Wire Line
	7000 1300 7200 1300
Wire Wire Line
	7000 1100 7200 1100
Wire Wire Line
	9700 1050 9550 1050
Wire Wire Line
	9550 950  9700 950 
Wire Wire Line
	8750 1050 8600 1050
Wire Wire Line
	8650 6100 8650 6000
Wire Wire Line
	8650 6000 8550 6000
Wire Wire Line
	8750 1150 8600 1150
Wire Wire Line
	9550 1150 9700 1150
Wire Wire Line
	6250 3000 6100 3000
Wire Wire Line
	7050 3000 7200 3000
Wire Wire Line
	7050 3300 7200 3300
Wire Wire Line
	7150 5000 7150 4900
Wire Wire Line
	7150 4900 7050 4900
Wire Wire Line
	7200 4800 7050 4800
Wire Wire Line
	6050 4800 6250 4800
Wire Wire Line
	6250 4900 6150 4900
Wire Wire Line
	6150 4900 6150 5000
Wire Wire Line
	7050 3200 7200 3200
Wire Wire Line
	7050 2900 7200 2900
Wire Wire Line
	6250 2900 6100 2900
Wire Wire Line
	9550 1250 9700 1250
Wire Wire Line
	8750 1250 8600 1250
Wire Wire Line
	7750 6000 7600 6000
Wire Wire Line
	8750 950  8600 950 
Wire Wire Line
	7000 1200 7200 1200
Wire Wire Line
	7000 1400 7200 1400
Wire Wire Line
	7000 1700 7200 1700
Wire Wire Line
	7200 1900 7000 1900
Wire Wire Line
	6200 2000 6000 2000
Wire Wire Line
	6200 1800 6000 1800
Wire Wire Line
	6200 1500 6000 1500
Wire Wire Line
	6200 1300 6000 1300
Wire Wire Line
	6200 1100 6000 1100
Wire Wire Line
	8750 1350 8600 1350
Wire Wire Line
	8750 1650 8600 1650
Wire Wire Line
	9550 1650 9700 1650
Wire Wire Line
	9700 1850 9550 1850
Wire Wire Line
	9700 2050 9550 2050
Wire Wire Line
	8750 1850 8600 1850
Wire Wire Line
	6250 2800 6100 2800
Wire Wire Line
	7050 2800 7200 2800
Wire Wire Line
	6250 3100 6100 3100
Wire Wire Line
	6100 3300 6250 3300
Wire Wire Line
	6250 3850 6150 3850
Wire Wire Line
	6250 4050 6150 4050
Wire Wire Line
	6150 4600 6250 4600
Wire Wire Line
	8200 2900 8050 2900
Wire Wire Line
	8200 3100 8050 3100
Wire Wire Line
	9000 2900 9150 2900
Wire Wire Line
	9000 3100 9150 3100
Wire Wire Line
	8225 3775 8125 3775
Wire Wire Line
	8225 4675 8125 4675
Wire Wire Line
	8225 3975 8125 3975
Wire Wire Line
	8125 4875 8225 4875
Wire Wire Line
	9275 4850 9100 4850
Wire Wire Line
	5450 6000 5600 6000
Wire Wire Line
	5450 5800 5600 5800
Wire Wire Line
	5450 5600 5600 5600
Wire Wire Line
	4650 5700 4500 5700
Wire Wire Line
	4650 5900 4500 5900
Wire Wire Line
	7075 6300 7200 6300
Wire Wire Line
	6275 6300 6150 6300
Wire Wire Line
	5450 6100 5725 6100
Wire Wire Line
	5725 6100 5725 6200
Wire Wire Line
	7650 5700 7750 5700
Wire Wire Line
	7750 5900 7650 5900
Wire Wire Line
	8675 5800 8550 5800
Text GLabel 8675 5900 2    45   Input ~ 0
USB_P4
Text GLabel 8675 5800 2    45   Input ~ 0
USB_P3
Text GLabel 8675 5700 2    45   Input ~ 0
USB_P2
Text GLabel 7650 5700 0    45   Input ~ 0
USB_M2
Text GLabel 7650 5800 0    45   Input ~ 0
USB_M3
Text GLabel 7650 5900 0    45   Input ~ 0
USB_M4
Text GLabel 5600 5700 2    45   Input ~ 0
TX
Text GLabel 4500 5700 0    45   Input ~ 0
RX
Text GLabel 5600 6000 2    45   Input ~ 0
USB_P4
Text GLabel 5600 5900 2    45   Input ~ 0
USB_P3
Text GLabel 5600 5800 2    45   Input ~ 0
USB_P2
Text GLabel 4500 5800 0    45   Input ~ 0
USB_M2
Text GLabel 4500 5900 0    45   Input ~ 0
USB_M3
Text GLabel 4500 6000 0    45   Input ~ 0
USB_M4
$Comp
L +5V #PWR01
U 1 1 51719AEC
P 4400 6100
F 0 "#PWR01" H 4400 6190 20  0001 C CNN
F 1 "+5V" H 4400 6190 30  0000 C CNN
	1    4400 6100
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR02
U 1 1 51719A7C
P 5725 6200
F 0 "#PWR02" H 5725 6200 30  0001 C CNN
F 1 "GND" H 5725 6130 30  0001 C CNN
	1    5725 6200
	1    0    0    -1  
$EndComp
$Comp
L CONN_6X2 P10
U 1 1 517197E0
P 5050 5850
F 0 "P10" H 5050 6200 60  0000 C CNN
F 1 "CONN_6X2" V 5050 5850 60  0000 C CNN
	1    5050 5850
	1    0    0    -1  
$EndComp
Text GLabel 9100 4850 0    45   Input ~ 0
M11
$Comp
L CONN_2 P105
U 1 1 516C331C
P 8575 4775
F 0 "P105" V 8525 4775 40  0000 C CNN
F 1 "CONN_2" V 8625 4775 40  0000 C CNN
	1    8575 4775
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 P106
U 1 1 516C331B
P 9625 4750
F 0 "P106" V 9575 4750 40  0000 C CNN
F 1 "CONN_2" V 9675 4750 40  0000 C CNN
	1    9625 4750
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 P104
U 1 1 516C330C
P 9650 3875
F 0 "P104" V 9600 3875 40  0000 C CNN
F 1 "CONN_2" V 9700 3875 40  0000 C CNN
	1    9650 3875
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 P103
U 1 1 516C32FA
P 8575 3875
F 0 "P103" V 8525 3875 40  0000 C CNN
F 1 "CONN_2" V 8625 3875 40  0000 C CNN
	1    8575 3875
	1    0    0    -1  
$EndComp
Text GLabel 9100 4650 0    45   Input ~ 0
M12
Text GLabel 8125 4875 0    45   Input ~ 0
M21
Text GLabel 8125 4675 0    45   Input ~ 0
M22
Text GLabel 9200 3975 0    45   Input ~ 0
M31
Text GLabel 9200 3775 0    45   Input ~ 0
M32
Text GLabel 8125 3975 0    45   Input ~ 0
M41
Text GLabel 8125 3775 0    45   Input ~ 0
M42
Text GLabel 9150 3200 2    45   Input ~ 0
FP33
Text GLabel 9150 3100 2    45   Input ~ 0
FP28
Text GLabel 9150 3000 2    45   Input ~ 0
FP7
Text GLabel 9150 2900 2    45   Input ~ 0
FP6
Text GLabel 8050 3200 0    45   Input ~ 0
FP17
Text GLabel 8050 3100 0    45   Input ~ 0
FP29
Text GLabel 8050 3000 0    45   Input ~ 0
DAC2
Text GLabel 8050 2900 0    45   Input ~ 0
DAC1
$Comp
L CONN_4X2 P102
U 1 1 516C30E1
P 8600 3050
F 0 "P102" H 8600 3300 50  0000 C CNN
F 1 "CONN_4X2" V 8600 3050 40  0000 C CNN
	1    8600 3050
	1    0    0    -1  
$EndComp
NoConn ~ 7050 4700
NoConn ~ 7050 4600
NoConn ~ 7050 4500
NoConn ~ 7050 4150
NoConn ~ 7050 4050
NoConn ~ 7050 3950
NoConn ~ 7050 3850
Text GLabel 6150 4700 0    45   Input ~ 0
M11
Text GLabel 6150 4600 0    45   Input ~ 0
M12
Text GLabel 6150 4500 0    45   Input ~ 0
M21
Text GLabel 6150 4150 0    45   Input ~ 0
M22
Text GLabel 6150 4050 0    45   Input ~ 0
M31
Text GLabel 6150 3950 0    45   Input ~ 0
M32
Text GLabel 6150 3850 0    45   Input ~ 0
M41
NoConn ~ 7050 3750
Text GLabel 6150 3750 0    45   Input ~ 0
M42
$Comp
L CONN_5X2 P5
U 1 1 515E0A89
P 6650 3950
F 0 "P5" H 6650 4250 60  0000 C CNN
F 1 "CONN_5X2" V 6650 3950 50  0000 C CNN
	1    6650 3950
	1    0    0    -1  
$EndComp
Text GLabel 7200 3100 2    45   Input ~ 0
FP29
Text GLabel 6100 3300 0    45   Input ~ 0
FP33
Text GLabel 6100 3200 0    45   Input ~ 0
FP17
Text GLabel 6100 3100 0    45   Input ~ 0
FP28
Text GLabel 7200 2800 2    45   Input ~ 0
FP7
Text GLabel 7200 2700 2    45   Input ~ 0
DAC2
Text GLabel 6100 2800 0    45   Input ~ 0
FP6
Text GLabel 6100 2700 0    45   Input ~ 0
DAC1
NoConn ~ 6250 3400
Text GLabel 7200 2000 2    45   Input ~ 0
3.3V
Text GLabel 7200 1900 2    45   Input ~ 0
3.3V
Text GLabel 7200 1800 2    45   Input ~ 0
FP31
Text GLabel 7200 1700 2    45   Input ~ 0
FP16
Text GLabel 6000 2000 0    45   Input ~ 0
TIERRA
Text GLabel 6000 1900 0    45   Input ~ 0
TIERRA
Text GLabel 6000 1800 0    45   Input ~ 0
FP30
Text GLabel 6000 1700 0    45   Input ~ 0
FP19
Text GLabel 7200 1500 2    45   Input ~ 0
FP18
Text GLabel 7200 1400 2    45   Input ~ 0
I2C1_SCL
Text GLabel 7200 1300 2    45   Input ~ 0
FP42
Text GLabel 6000 1500 0    45   Input ~ 0
I2C1_SDA
Text GLabel 6000 1400 0    45   Input ~ 0
FP35
Text GLabel 6000 1300 0    45   Input ~ 0
FP41
Text GLabel 9700 2050 2    45   Input ~ 0
3.3V
Text GLabel 9700 1950 2    45   Input ~ 0
TIERRA
Text GLabel 8600 1850 0    45   Input ~ 0
FP30
Text GLabel 8600 1750 0    45   Input ~ 0
FP18
Text GLabel 9700 1850 2    45   Input ~ 0
FP31
Text GLabel 9700 1750 2    45   Input ~ 0
FP16
Text GLabel 8600 1650 0    45   Input ~ 0
FP19
Text GLabel 9700 1650 2    45   Input ~ 0
I2C1_SCL
Text GLabel 8600 1550 0    45   Input ~ 0
I2C1_SDA
NoConn ~ 9550 1550
NoConn ~ 8750 1450
Text GLabel 9700 1450 2    45   Input ~ 0
FP35
Text GLabel 9700 1350 2    45   Input ~ 0
FP42
Text GLabel 8600 1350 0    45   Input ~ 0
FP41
$Comp
L CONN_12X2 P7
U 1 1 515DE467
P 9150 1500
F 0 "P7" H 9150 2150 60  0000 C CNN
F 1 "CONN_12X2" V 9150 1500 50  0000 C CNN
	1    9150 1500
	-1   0    0    1   
$EndComp
Text GLabel 7200 1200 2    45   Input ~ 0
FP2
Text GLabel 7200 1100 2    45   Input ~ 0
FP3
Text GLabel 9700 1050 2    45   Input ~ 0
FP1
Text GLabel 9700 950  2    45   Input ~ 0
FP3
Text GLabel 8600 950  0    45   Input ~ 0
FP2
Text GLabel 6000 1100 0    45   Input ~ 0
FP1
Text GLabel 6000 1200 0    45   Input ~ 0
FP0
Text GLabel 8600 1050 0    50   Input ~ 0
FP0
$Comp
L CONN_4X2 P101
U 1 1 516C28B0
P 6600 1850
F 0 "P101" H 6600 2100 50  0000 C CNN
F 1 "CONN_4X2" V 6600 1850 40  0000 C CNN
	1    6600 1850
	1    0    0    -1  
$EndComp
$Comp
L CONN_5X2 P100
U 1 1 516C289B
P 6600 1300
F 0 "P100" H 6600 1600 60  0000 C CNN
F 1 "CONN_5X2" V 6600 1300 50  0000 C CNN
	1    6600 1300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR03
U 1 1 515E0D9F
P 7600 6000
F 0 "#PWR03" H 7600 6090 20  0001 C CNN
F 1 "+5V" V 7600 6150 30  0000 C CNN
	1    7600 6000
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR04
U 1 1 515E0D7A
P 8650 6100
F 0 "#PWR04" H 8650 6100 30  0001 C CNN
F 1 "GND" H 8650 6030 30  0001 C CNN
	1    8650 6100
	1    0    0    -1  
$EndComp
$Comp
L CONN_4X2 P2
U 1 1 515DE49F
P 8150 5850
F 0 "P2" H 8150 6100 50  0000 C CNN
F 1 "CONN_4X2" V 8150 5850 40  0000 C CNN
	1    8150 5850
	1    0    0    -1  
$EndComp
$Comp
L CONN_5X2 P4
U 1 1 515E0A8E
P 6650 4700
F 0 "P4" H 6650 5000 60  0000 C CNN
F 1 "CONN_5X2" V 6650 4700 50  0000 C CNN
	1    6650 4700
	1    0    0    -1  
$EndComp
Text GLabel 9700 1250 2    45   Input ~ 0
AMP3
Text GLabel 9700 1150 2    45   Input ~ 0
AMP2
Text GLabel 8600 1250 0    45   Input ~ 0
AMP4
Text GLabel 8600 1150 0    45   Input ~ 0
AMP1
NoConn ~ 10025 5300
NoConn ~ 10025 5400
NoConn ~ 10025 5500
NoConn ~ 10025 5600
NoConn ~ 10025 5700
NoConn ~ 10025 5800
NoConn ~ 10025 5900
NoConn ~ 10025 6000
NoConn ~ 10025 6100
NoConn ~ 10025 6200
NoConn ~ 10025 6300
NoConn ~ 10025 6400
NoConn ~ 9225 6400
NoConn ~ 9225 6300
NoConn ~ 9225 6200
NoConn ~ 9225 6100
NoConn ~ 9225 6000
NoConn ~ 9225 5900
NoConn ~ 9225 5800
NoConn ~ 9225 5700
NoConn ~ 9225 5600
NoConn ~ 9225 5500
NoConn ~ 9225 5400
NoConn ~ 9225 5300
NoConn ~ 8750 2050
NoConn ~ 8750 1950
Text GLabel 6100 3000 0    45   Input ~ 0
ON PULSE
Text GLabel 6100 2900 0    45   Input ~ 0
AMP5
Text GLabel 7200 2900 2    45   Input ~ 0
AMP6
Text GLabel 7200 3000 2    45   Input ~ 0
LATCH_LEDS
Text GLabel 7200 3200 2    45   Input ~ 0
SHIFT_LED
Text GLabel 7200 3300 2    45   Input ~ 0
SERIAL_IN
NoConn ~ 7050 3400
$Comp
L GND #PWR05
U 1 1 515DE7DC
P 6150 5000
F 0 "#PWR05" H 6150 5000 30  0001 C CNN
F 1 "GND" H 6150 4930 30  0001 C CNN
	1    6150 5000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 515DE7D8
P 7150 5000
F 0 "#PWR06" H 7150 5000 30  0001 C CNN
F 1 "GND" H 7150 4930 30  0001 C CNN
	1    7150 5000
	1    0    0    -1  
$EndComp
Text GLabel 6050 4800 0    45   Input ~ 0
VMOTOR
Text GLabel 7200 4800 2    45   Input ~ 0
VMOTOR
NoConn ~ 6275 5300
NoConn ~ 6275 5400
NoConn ~ 6275 5500
NoConn ~ 6275 5600
NoConn ~ 6275 5700
NoConn ~ 6275 5800
NoConn ~ 6275 5900
NoConn ~ 6275 6000
NoConn ~ 6275 6100
NoConn ~ 6275 6200
NoConn ~ 7075 5300
NoConn ~ 7075 5400
NoConn ~ 7075 5500
NoConn ~ 7075 5600
NoConn ~ 7075 5700
NoConn ~ 7075 5800
NoConn ~ 7075 5900
NoConn ~ 7075 6000
NoConn ~ 7075 6100
NoConn ~ 7075 6200
$Comp
L CONN_8X2 P6
U 1 1 515DE48C
P 6650 3050
F 0 "P6" H 6650 3500 60  0000 C CNN
F 1 "CONN_8X2" V 6650 3050 50  0000 C CNN
	1    6650 3050
	1    0    0    -1  
$EndComp
$Comp
L CONN_12X2 P3
U 1 1 515DE460
P 9625 5850
F 0 "P3" H 9625 6500 60  0000 C CNN
F 1 "CONN_12X2" V 9625 5850 50  0000 C CNN
	1    9625 5850
	1    0    0    -1  
$EndComp
$Comp
L CONN_12X2 P1
U 1 1 5158B543
P 6675 5850
F 0 "P1" H 6675 6500 60  0000 C CNN
F 1 "CONN_12X2" V 6675 5850 50  0000 C CNN
	1    6675 5850
	1    0    0    -1  
$EndComp
$Sheet
S 1950 4900 1250 2050
U 5112AC2A
F0 "POWER" 60
F1 "Source.sch" 60
$EndSheet
$Sheet
S 3750 1550 1200 2400
U 5112ABF3
F0 "SENSOR" 60
F1 "Sensores.sch" 60
$EndSheet
$Sheet
S 1900 1550 1250 2450
U 5112ABB8
F0 "LEDS" 60
F1 "leds.sch" 60
$EndSheet
$EndSCHEMATC
