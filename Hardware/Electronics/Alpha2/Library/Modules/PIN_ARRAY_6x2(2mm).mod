PCBNEW-LibModule-V1  vie 01 nov 2013 13:01:05 COT
# encoding utf-8
Units mm
$INDEX
pin_array_6x2(2mm)
$EndINDEX
$MODULE pin_array_6x2(2mm)
Po 0 0 0 15 5273EC50 00000000 ~~
Li pin_array_6x2(2mm)
Cd Double rangee de contacts 2 x 12 pins
Kw CONN
Sc 0
AR 
Op 0 0 0
T0 0 -3.81 1.016 1.016 0 0.254 N V 21 N "PIN_ARRAY_6X2(2mm)"
T1 0 4 1.016 1.016 0 0.2032 N V 21 N "Val**"
DS -6 2 6 2 0.3 21
DS 6 2 6 -2 0.3 21
DS 6 -2 -6 -2 0.3 21
DS -6 -2 -6 2 0.3 21
$PAD
Sh "2" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5 -1
$EndPAD
$PAD
Sh "1" R 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5 1
$EndPAD
$PAD
Sh "3" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3 1
$EndPAD
$PAD
Sh "4" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3 -1
$EndPAD
$PAD
Sh "5" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1 1
$EndPAD
$PAD
Sh "6" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1 -1
$EndPAD
$PAD
Sh "7" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1 1
$EndPAD
$PAD
Sh "8" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1 -1
$EndPAD
$PAD
Sh "9" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3 1
$EndPAD
$PAD
Sh "10" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3 -1
$EndPAD
$PAD
Sh "11" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5 1
$EndPAD
$PAD
Sh "12" C 1.5 1.5 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5 -1
$EndPAD
$EndMODULE pin_array_6x2(2mm)
$EndLIBRARY
