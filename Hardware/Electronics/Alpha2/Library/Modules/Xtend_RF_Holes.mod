PCBNEW-LibModule-V1  mar 30 abr 2013 11:48:09 COT
# encoding utf-8
Units mm
$INDEX
Xtend_RF_Holes
$EndINDEX
$MODULE Xtend_RF_Holes
Po 0 0 0 15 517FF5C1 00000000 ~~
Li Xtend_RF_Holes
Sc 0
AR 
Op 0 0 0
T0 0 0 1 1 0 0.15 N V 21 N "Xtend_RF_Holes"
T1 0 2 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "1" C 3.6 3.6 0 0 0
Dr 3.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -14 0
$EndPAD
$PAD
Sh "2" C 3.6 3.6 0 0 0
Dr 3.3 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 15 0
$EndPAD
$EndMODULE Xtend_RF_Holes
$EndLIBRARY
