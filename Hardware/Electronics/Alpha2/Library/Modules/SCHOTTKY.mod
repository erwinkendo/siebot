PCBNEW-LibModule-V1  dom 13 oct 2013 17:32:13 COT
# encoding utf-8
Units mm
$INDEX
SCHOTTKY
$EndINDEX
$MODULE SCHOTTKY
Po 0 0 0 15 525B1F67 00000000 ~~
Li SCHOTTKY
Sc 0
AR 
Op 0 0 0
T0 2.1844 3.8608 1.524 1.524 0 0.20066 N V 21 N "SCHOTTKY"
T1 2.4384 -3.7592 1.524 1.524 0 0.20066 N I 21 N "VAL**"
DS 2.9972 -0.0508 3.9624 -0.0508 0.09906 21
DS 1.2192 -0.0508 0.2032 -0.0508 0.09906 21
DS 2.9972 -1.0668 2.4892 -1.0668 0.09906 21
DS 2.9972 -1.0668 2.9972 0.8128 0.09906 21
DS 2.9972 0.8128 2.9972 0.9652 0.09906 21
DS 2.9972 0.9652 3.4544 0.9652 0.09906 21
DS 1.3208 -0.7112 1.3208 0.7112 0.09906 21
DS 1.3208 0.7112 3.048 -0.0508 0.09906 21
DS 3.048 -0.0508 1.27 -0.7112 0.09906 21
DS 1.27 -0.7112 1.27 0.7112 0.09906 21
$PAD
Sh "1" R 2.5 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "2" R 2.5 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.3 0
$EndPAD
$EndMODULE SCHOTTKY
$EndLIBRARY
