PCBNEW-LibModule-V1  dom 13 oct 2013 20:49:38 COT
# encoding utf-8
Units mm
$INDEX
DDECONN
U3
$EndINDEX
$MODULE DDECONN
Po 0 0 0 15 525B3963 00000000 ~~
Li DDECONN
Sc 0
AR /525B474A
Op 0 0 0
T0 1.2192 4.064 1.524 1.524 0 0.20066 N V 21 N "P9"
T1 1.3716 -3.6068 1 1 0 0.20066 N V 21 N "MOTOR"
DC -0.2032 1.8796 -0.1016 2.286 0.09906 21
DS -1.27 -2.8448 3.81 -2.8448 0.09906 21
DS -1.27 2.8448 3.81 2.8448 0.09906 21
DS -1.28 -2.86 -1.28 2.86 0.09906 21
DS 3.82 2.86 3.82 -2.86 0.09906 21
$PAD
Sh "1" C 1.9 1.9 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 1 "/+V_MOTOR"
Po 0 0
$EndPAD
$PAD
Sh "2" C 1.9 1.9 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 2 "/-V_MOTOR"
Po 2.54 0
$EndPAD
$EndMODULE DDECONN
$MODULE U3
Po 0 0 0 15 525B4D91 00000000 ~~
Li U3
Sc 0
AR /525AD3C9
Op 0 0 0
T0 -1.27 -2.032 0.5 0.5 0 0.125 N V 21 N "U3"
T1 3.048 -2.032 0.5 0.5 900 0.125 N V 21 N "TPS62046DGQ"
DC -0.0508 -0.9144 0 -0.8128 0.09906 21
DS -0.4064 -3.4544 -0.4064 -0.5588 0.09906 21
DS -0.4064 -3.4544 2.3368 -3.4544 0.09906 21
DS 2.3368 -3.4544 2.3368 -0.5588 0.09906 21
DS 2.3368 -0.5588 -0.254 -0.5588 0.09906 21
DS -0.254 -0.5588 -0.3556 -0.5588 0.09906 21
DS -0.3556 -0.5588 -0.3556 -2.286 0.09906 21
DS -0.3556 -2.286 -0.3556 -3.4544 0.09906 21
$PAD
Sh "1" R 0.27 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "/5V3"
Po 0 0
$EndPAD
$PAD
Sh "2" R 0.27 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "/5V3"
Po 0.5 0
$EndPAD
$PAD
Sh "3" R 0.27 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "/5V3"
Po 1 0
$EndPAD
$PAD
Sh "4" R 0.27 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "GND"
Po 1.5 0
$EndPAD
$PAD
Sh "5" R 0.27 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "+3.3VP"
Po 2 0
$EndPAD
$PAD
Sh "6" R 0.27 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "GND"
Po 2 -4
$EndPAD
$PAD
Sh "7" R 0.27 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 4 "N-0000045"
Po 1.5 -4
$EndPAD
$PAD
Sh "8" R 0.27 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 4 "N-0000045"
Po 1 -4
$EndPAD
$PAD
Sh "9" R 0.27 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "GND"
Po 0.5 -4
$EndPAD
$PAD
Sh "10" R 0.27 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "GND"
Po 0 -4
$EndPAD
$PAD
Sh "11" R 1.88 1.57 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1 -2.1
$EndPAD
$PAD
Sh "12" R 10 10 0 0 0
Dr 0 0 0
At SMD N 00880001
Ne 0 ""
Po 1 -2.1
$EndPAD
$PAD
Sh "13" C 1 1 0 0 0
Dr 0.5 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1 -2.1
$EndPAD
$EndMODULE U3
$EndLIBRARY
